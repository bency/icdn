<?php
include dirname(__FILE__) . '/../include/mysql.inc.php';
header("Content-Type: application/json; charset=utf-8", true);

if(isset($_POST['db'])){

  $time = isset($_POST['time'])? $_POST['time'] : 0;

  $dba = Mysql::get_db($_POST['db']);
  $sv_log = null;
  if($sv_stmt = $dba->query("select * from server_table where sys_time=$time;")){
      $sv_log = array();
      $i = 0;
      while($q = $sv_stmt->fetch(PDO::FETCH_OBJ)){
          $sv_log[$i]['sys_time'] = (int)$q->sys_time;
          $sv_log[$i]['sid'] = (int)$q->sid;
          $sv_log[$i]['state'] = $q->state;

          $content = $q->content;
          $p_sv_tmp = explode(';',$content);
          $pids = array();
          foreach($p_sv_tmp as $pvc){
              $p = explode(',',$pvc);
              if(isset($p[1]))
              $pids[$p[0]] = $p[1];
          }

          asort($pids);
          $start = 0;
          $end = 0;
          $tmp_ar = array();
          $j=0;
          foreach($pids as $pid => $count){
              //$tmp_ar[] = array($q->sid . ":" . $q->state, "PID: " . $pid, "Mon Jan 1 $start 00:00:00 GMT+0800 (CST)", "Mon Jan 1 $end 00:00:00 GMT+0800 (CST)");
              $tmp_ar[$j]['pid'] = (int)$pid;
              $tmp_ar[$j++]['counter'] = (int)$count;
          }
          $sv_log[$i]['content'] = $tmp_ar;
          $i++;
      }
      echo (json_encode($sv_log));
  }else{
    echo ($dba->errorInfo());
  }
}
