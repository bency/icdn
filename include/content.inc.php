<?php

$pid = isset($_GET['pid']) ? $_GET['pid'] : 0;

switch($pid){
    case '1':
        include dirname(__FILE__) . '/status.inc.php';
        break;
    case '2':
        include dirname(__FILE__) . '/single.inc.php';
        break;
    default:
        include dirname(__FILE__) . '/index.inc.php';
        break;
}
