<?php
include dirname(__FILE__) . '/../vendor/autoload.php';
include dirname(__FILE__) . '/mysql.inc.php';
include dirname(__FILE__) . '/../simulate/function.inc.php';
date_default_timezone_set("Asia/Taipei");
$loader = new Twig_Loader_Filesystem('template');
$twig = new Twig_Environment($loader, array(
            'cache' => 'cache',
            'debug' => true,
            ));
$twig->addExtension(new Twig_Extension_Debug());
$twig->clearCacheFiles();

$dba = Mysql::get_db();
$m = $dba->query("show databases");
$dbss = null;
$pattern = '/CDN_.*/';
if(isset($_POST['submit_filter'])){
    if(isset($_POST['life'])){
        if(is_array($_POST['life'])){
            $tmp = implode("|",$_POST['life']);
            $life_ptn = '(' . $tmp . ')';
        }else{
            $life_ptn = $_POST['life'];
        }
    }else{
        $life_ptn = '[0-9].*';
    }
    if(isset($_POST['z'])){
        if(is_array($_POST['z'])){
            $tmp = implode("|",$_POST['z']);
            $z_ptn = '(' . $tmp . ')';
        }else{
            $z_ptn = $_POST['z'];
        }
    }else{
        $z_ptn = '[0-9].*';
    }
    if(isset($_POST['pids'])){
        if(is_array($_POST['pids'])){
            $tmp = implode("|",$_POST['pids']);
            $pids_ptn = '(' . $tmp . ')';
        }else{
            $pids_ptn = $_POST['pids'];
        }
    }else{
        $pids_ptn = '[0-9].*';
    }
    if(isset($_POST['mp'])){
        if(is_array($_POST['mp'])){
            $tmp = implode("|",$_POST['mp']);
            $mp_ptn = '(' . $tmp . ')';
        }else{
            $mp_ptn = $_POST['mp'];
        }
    }else{
        $mp_ptn = '[0-9].*';
    }
    $pattern2 = '/CDN_' . $life_ptn . '_' . $z_ptn . '_' . $pids_ptn . '_' . $mp_ptn . '/';
}
else{
    $pattern2 = $pattern;
}
while($q = $m->fetch(PDO::FETCH_OBJ)){
    if(preg_match($pattern,$q->Database))
        $dbss[] = $q->Database;
}
natsort($dbss);
$t_life = array();
$t_z = array();
$t_pids = array();
$t_mp = array();
$db_param = null;
foreach($dbss as $db){
 
    if(preg_match($pattern2,$db))
        $dbs[] = $db;
    $db_param = explode("_",$db);
    if(!isset($t_life[$db_param[1]]))
        $t_life[$db_param[1]] = 1;
    if(!isset($t_z[$db_param[2]]))
        $t_z[$db_param[2]] = 1;
    if(!isset($t_pids[$db_param[3]]))
        $t_pids[$db_param[3]] = 1;
    if(!isset($t_mp[$db_param[4]]))
        $t_mp[$db_param[4]] = 1;
}
$_life = array_keys($t_life);
natsort($_life);
$_z = array_keys($t_z);
natsort($_z);
$_pids = array_keys($t_pids);
natsort($_pids);
$_mp = array_keys($t_mp);
natsort($_mp);
$i = 0;
$server_comp = array();
$server_comp_em = array();
$log_count = array();
$system_table = array();
$percentage = array();
foreach( $dbs as $db ){
    $dba = Mysql::get_db($db);
    $stmt = $dba->query("select * from log_table where num_sv_total > 0");
    $settings_stmt = $dba->query("select setting, value from system_table");
    $setting = null;
    $setting = array();
    while($settings = $settings_stmt->fetch()){
        $setting[$settings[0]] = $settings[1];
    }
    $pid_per_server = $setting['SPACE_PER_SERVER'] / $setting['SPACE_PER_PROGRAM'];
    $conn_per_server = $setting['BW_PER_SERVER'] / $setting['BW_PER_PROGRAM'];
    $arrival_mul = isset($setting['ARRIVAL_MUL'])? $setting['ARRIVAL_MUL'] : 1;
    $log_table = array();
    $j = 0;
    $sc[] = array("sys_time","bwf","spf","full","total","理論值","total conns(k)","file add(k)");
    $sc_em[] = array("sys_time","bwf","spf","full","total","理論值spf","理論值bwf","total conns(k)","file add(k)");
    $num_sv_b = 0;
    $num_sv_s = 0;
    $num_sv_full = 0;
    $num_sv_total = 0;
    $num_pid_total = 0;
    $file = 0;
    $opt = 0;
    $conns = 0;
    while($q = $stmt->fetch(PDO::FETCH_OBJ)){
        $log_table[$i][$j]['sys_time'] = $q->sys_time;
        $log_table[$i][$j]['num_sv_total'] = $q->num_sv_total;
        $log_table[$i][$j]['num_sv_b'] = $q->num_sv_b;
        $log_table[$i][$j]['num_sv_s'] = $q->num_sv_s;
        $log_table[$i][$j]['num_sv_f'] = $q->num_sv_f;
        $log_table[$i][$j]['num_conn_total'] = $q->num_conn_total;
        $log_table[$i][$j]['num_pid_total'] = $q->num_pid_total;
        $log_table[$i][$j++]['file_add'] = $q->file_add;
        if($q->num_conn_total / $conn_per_server > $q->num_pid_total / $pid_per_server ){
            $opt += ceil((int)$q->num_conn_total / $conn_per_server);
            $sc_em[] = array($q->sys_time, (int)$q->num_sv_b, (int)$q->num_sv_s, (int)$q->num_sv_f, (int)$q->num_sv_total, 0, ceil((int)$q->num_conn_total / $conn_per_server),$arrival_mul * arrival_in_minute($j)/1000,(int)$q->file_add/1000);
        }
        else{
            $opt += ceil((int)$q->num_pid_total / $pid_per_server);
            $sc_em[] = array($q->sys_time, (int)$q->num_sv_b, (int)$q->num_sv_s, (int)$q->num_sv_f, (int)$q->num_sv_total, ceil((int)$q->num_pid_total / $pid_per_server), 0, $arrival_mul * arrival_in_minute($j)/1000,(int)$q->file_add/1000);
        }
        $num_sv_b += (int)$q->num_sv_b;
        $num_sv_s += (int)$q->num_sv_s;
        $num_sv_full += (int)$q->num_sv_f;
        $num_sv_total += (int)$q->num_sv_total;
        $file += (int)$q->file_add;
        $conns += arrival_in_minute($j);
        if($j%60 == 59){
            $sc[] = array(($j+1)/60,$num_sv_b/60, $num_sv_s/60, $num_sv_full/60, $num_sv_total/60, $opt/60, $conns/60/1000, $file/60/1000);
            $num_sv_b = 0;
            $num_sv_s = 0;
            $num_sv_full = 0;
            $num_sv_total = 0;
            $num_pid_total = 0;
            $opt = 0;
            $file = 0;
            $conns = 0;
        }
        
    }
    $log_count[$i] = $j;
    $server_comp[$i] = json_encode($sc);
    $server_comp_em[$i] = json_encode($sc_em);
    if($j < 1440){
      $percentage[$i] = $j / 14.40;
    }else{
      $percentage[$i] = 100;
    }
    $sc = null;
    $sc_em = null;
    $stmt->closeCursor();
    if($set_stmt = $dba->query("select setting,value from system_table")){
        $k = 0;
        while($q = $set_stmt->fetch(PDO::FETCH_OBJ)){
            $system_table[$i][$k]['setting'] = $q->setting;
            if($q->setting == "TIME_START"){
                $system_table[$i][$k++]['value'] = date("Y-m-d h:i",$q->value);
            }else{
                $system_table[$i][$k++]['value'] = $q->value;
            }
        }
    }
    $i++;
}
echo $twig->render('index.html',
        array('title' => 'Bency 私人站方@' . exec('hostname'),
            'pid' => $pid,
            'log_table' => $log_table,
            'server_comp' => $server_comp,
            'server_comp_em' => $server_comp_em,
            'log_count' => $log_count,
            'system_table' => $system_table,
            'width' => $i * 20,
            'percentage' => $percentage,
            'dbs' => $dbs,
            '_life' => $_life,
            '_z' => $_z,
            '_pids' => $_pids,
            '_mp' => $_mp
            )
        );
