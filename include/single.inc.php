<?php
include dirname(__FILE__) . '/../vendor/autoload.php';
include dirname(__FILE__) . '/mysql.inc.php';
include dirname(__FILE__) . '/../simulate/function.inc.php';
date_default_timezone_set("Asia/Taipei");
$loader = new Twig_Loader_Filesystem('template');
$twig = new Twig_Environment($loader, array(
            'cache' => 'cache',
            'debug' => true,
            ));
$twig->addExtension(new Twig_Extension_Debug());
$twig->clearCacheFiles();

$dba = Mysql::get_db();
$m = $dba->query("show databases");
$db = null;
while($q = $m->fetch(PDO::FETCH_OBJ)){
    if(preg_match('/CDN_.*/',$q->Database))
        $db[] = $q->Database;
}

if(count($db) > 0 and !isset($_POST['submit']) ){
    $_POST['submit'] = true;
    $_POST['db'] = $db[0];
}

$dba = Mysql::get_db($_POST['db']);
$system_table = null;
if($set_stmt = $dba->query("select setting,value from system_table")){
    $system_table = array();
    $j = 0;
    while($q = $set_stmt->fetch(PDO::FETCH_LAZY)){
            $setting[$q[0]] = $q[1];
            $system_table[$j]['setting'] = $q->setting;
            if($q->setting == "TIME_START"){
                $system_table[$j++]['value'] = date("Y-m-d h:i",$q->value);
            }else{
                $system_table[$j++]['value'] = $q->value;
            }
    }
}
$pid_per_server = $setting['SPACE_PER_SERVER'] / $setting['SPACE_PER_PROGRAM'];
$conn_per_server = $setting['BW_PER_SERVER'] / $setting['BW_PER_PROGRAM'];
$arrival_mul = isset($setting['ARRIVAL_MUL'])? $setting['ARRIVAL_MUL'] : 1;
$stmt = $dba->query("select * from log_table where num_sv_total > 0");
$log_table = array();
$i = 0;
$server_comp = array();
$server_comp[] = array("sys_time","bwf","spf","full","total","理論值spf","理論值bwf","total conns(k)","file add(k)");
while($q = $stmt->fetch(PDO::FETCH_OBJ)){
    $log_table[$i]['sys_time'] = $q->sys_time;
    $log_table[$i]['num_sv_total'] = $q->num_sv_total;
    $log_table[$i]['num_sv_b'] = $q->num_sv_b;
    $log_table[$i]['num_sv_s'] = $q->num_sv_s;
    $log_table[$i]['num_sv_f'] = $q->num_sv_f;
    $log_table[$i]['num_conn_total'] = $q->num_conn_total;
    $log_table[$i]['num_pid_total'] = $q->num_pid_total;
    $log_table[$i]['file_add'] = $q->file_add;
    if($q->num_conn_total / $conn_per_server > $q->num_pid_total / $pid_per_server )
        $server_comp[] = array($q->sys_time, (int)$q->num_sv_b, (int)$q->num_sv_s, (int)$q->num_sv_f, (int)$q->num_sv_total, 0, ceil((int)$q->num_conn_total/$conn_per_server),$arrival_mul * arrival_in_minute($i++)/1000,(int)$q->file_add/1000);
    else
        $server_comp[] = array($q->sys_time, (int)$q->num_sv_b, (int)$q->num_sv_s, (int)$q->num_sv_f, (int)$q->num_sv_total, ceil((int)$q->num_pid_total/$pid_per_server),0,$arrival_mul * arrival_in_minute($i++)/1000,(int)$q->file_add/1000);
}

$stmt->closeCursor();
echo $twig->render('single.html',
        array('title' => 'Bency 私人站方@' . exec('hostname'),
            'pid' => $pid,
            'log_table' => $log_table,
            'server_comp' => json_encode($server_comp),
            'selecteddb' => $_POST['db'],
            'system_table' => $system_table,
            'width' => $i * 20,
            'dbs' => $db
            )
        );
