<?php
include dirname(__FILE__) . '/../vendor/autoload.php';
$loader = new Twig_Loader_Filesystem('template');
$twig = new Twig_Environment($loader, array(
            'cache' => 'cache',
            'debug' => true,
            ));
$twig->addExtension(new Twig_Extension_Debug());
$twig->clearCacheFiles();
$i = 0;
$server = array();
foreach($_SERVER as $key => $value){
    $server[$i]['service'] = $key;
    $server[$i++]['content'] = $value;
}
echo $twig->render('status.html',
        array('title' => 'Bency 私人站方@' . exec('hostname'),
            'service' => $server,
            'pid' => $pid
            )
        );
