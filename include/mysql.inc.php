<?php
class Mysql {
    
    private static $db = null;

    /*
    *   Refenenced by: http://tw1.php.net/manual/en/class.pdo.php#97682
    */
    public static function get_db($dbname = null){
        if( self::$db and $dbname == null){
            return self::$db;
        }else{
            self::$db = null;
        }
        $user = 'root';
        $passwd = 'ddyoinejjmj';
        $database = isset($dbname) ? $dbname : 'mysql';
        try{
            self::$db = new PDO( "mysql:host=localhost;dbname=" . $database, $user, $passwd );
        }
        catch ( PDOException $e ){
              print "Error!: " . $e->getMessage() . "<br/>";
              die();
        }

        return self::$db;
    }

    public function __call( $name, $args){
        
        $callback = array( self::get_db(), $name );
        return call_user_func_array( $callback, $args );
    }

    public function __destruct(){
    
        //Debug::func_end("link for $this->func_name");
    }
}
