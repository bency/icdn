<?php
// Last Update:2013/09/12 01:40:47 

class connHeap extends SplHeap{

    public function compare($a,$b){
        if($a->depart == $b->depart)
            return 0;
        return $a->depart > $b->depart ? -1 : 1;
    }
}

class Proc {
    
    public static $dep_from_spf = 0;
    public static $cnid = 0;
    public static $sid  = 0;
    public static $conn = array();
//    public static $next_depart = null;
    public static $NUM_CONN = 0;
    public static $file_add = 0;
    public static $servers = array();
    public static $spf = array();
    public static $bwf = array();
    public static $ops = array();
    public static $full = array();
    public static $cH = null;

    public function __call( $name, $arguments){
    
        echo "注意! 注意! class Proc 中 有個叫做'$name' 的方法還沒寫\n"
                     . implode(', ', $arguments). "\n";
    }

    public function __construct(){
        $sv = new Server();
        self::$servers[$sv->sid] = $sv;
        self::$ops = $sv;
        unset($sv);
        self::$cH = new connHeap();
    }

    public function add_conn($conn){
        
        self::$NUM_CONN ++;
        $conn->cnid = self::$cnid;
        //Debug::output("產生新連線 $conn->cnid pid: $conn->pid");
        //Debug::conn_state($conn);
        self::$cH->insert($conn);
        self::$conn[self::$cnid] = $conn;
        $added = false;
        //Debug::output("add_conn1");
        if(count(self::$spf) > 0){
        //Debug::output("add_conn2");
            foreach(self::$spf as $sv){
                if(isset($sv->plist[$conn->pid])){
                    //Debug::output("加入spf $sv->sid");
                    $sv->plist[$conn->pid][$conn->cnid] = $conn->cnid;
                    $conn->sid = $sv->sid;
                    $sv->bandwidth -= $conn->bandwidth;
                    if( $sv->bandwidth == 0){
                        $sv->state = 'F';
                        self::$full[$sv->sid] = $sv;
                        unset(self::$spf[$sv->sid]);
                    }
                    $added = true;
                    break;
                }
            }
        }
        if(!$added){
        //Debug::output("add_conn3");
            if(isset(self::$ops->sid)){
                //Debug::output("ops info");
                //Debug::server_state(self::$ops);
                //Debug::output("add_conn31");
                //Debug::output("加入ops ",self::$ops->sid);
                if(isset(self::$ops->plist[$conn->pid])){
                    //Debug::output("add_conn311");
                    $conn->sid = self::$ops->sid;
                    self::$ops->plist[$conn->pid][$conn->cnid] = $conn->cnid;
                    self::$ops->bandwidth -= $conn->bandwidth;
                    if( self::$ops->bandwidth == 0){
                        //Debug::output("ops改成bwf ".self::$ops->sid);
                        //Debug::output("add_conn3111");
                        self::$ops->state = 'B';
                        self::$bwf[self::$ops->sid] = self::$ops;
                        self::$ops = null;
                    }
                    $added = true;
                }else{
                    //Debug::output("add_conn312");
                    self::$ops->bandwidth -= $conn->bandwidth;
                    self::$ops->space     -= $conn->space;
                    self::$ops->plist[$conn->pid][$conn->cnid] = $conn->cnid;
                    $conn->sid = self::$ops->sid;
                    self::$file_add ++;
                    if( self::$ops->bandwidth == 0 and self::$ops->space == 0 ){
                        //Debug::output("add_conn3121");
                        self::$ops->state = 'F';
                        self::$full[self::$ops->sid] = self::$ops;
                        self::$ops = null;
                        self::$ops = new Server();
                    }else if( self::$ops->bandwidth == 0 and self::$ops->space > 0){
                        //Debug::output("add_conn3122");
                        self::$ops->state = 'B';
                        self::$bwf[self::$ops->sid] = self::$ops;
                        self::$ops = null;
                        self::$ops = new Server();
                    }else if( self::$ops->bandwidth > 0 and self::$ops->space == 0){
                        //Debug::output("add_conn3123");
                        self::$ops->state = 'S';
                        self::$spf[self::$ops->sid] = self::$ops;
                        self::$ops = null;
                        self::$ops = new Server();
                    }
                    $added = true;
                }
            }else{
                //Debug::output("新增server");
                //Debug::output("add_conn32");
                self::$ops = new Server();
                self::$ops->bandwidth -= $conn->bandwidth;
                self::$ops->space     -= $conn->space;
                self::$ops->plist[$conn->pid][$conn->cnid] = $conn->cnid ;
                self::$servers[self::$ops->sid] = self::$ops;
                $conn->sid = self::$ops->sid;
                $added = true;
                self::$file_add ++;
                //Debug::output("加到新server");
            }
        }
        //Debug::output("加入後的伺服器狀態");
        //Debug::server_state(self::$servers[$conn->sid]);
        self::$cnid++;
    }

    public function depart(){

        
        $departed = false;
        $next_depart = self::$cH->extract();
        $next_depart = self::$conn[$next_depart->cnid];
        //Debug::output("即將離開的連線");
        $xt_server = self::$servers[$next_depart->sid];
        $fm_server = self::$servers[$next_depart->sid];
        //Debug::conn_state($next_depart);
        //Debug::server_state($fm_server);
        /*
        if($fm_server->space < 0 or $fm_server->bandwidth < 0){
            Debug::error_report("頻寬或空間計算錯誤",$fm_server);
            exit;
        }
        */

        if($xt_server->state == "S"){
            
            // 先在ops裡面找屬於該pid的連線
            if(isset(self::$ops->plist[$next_depart->pid])){
                //Debug::output("從ops離開");
                $xt_server = self::$ops;
            }
        }elseif($xt_server->state == "F"){

            // 先在ops裡面找屬於該pid的連線
            if(isset(self::$ops->plist[$next_depart->pid])){
                //Debug::output("從ops離開");
                $xt_server = self::$ops;
            }else{

                foreach(self::$spf as $sv){
                    if(array_key_exists($next_depart->pid,$sv->plist)){
                        //Debug::output("從spf離開");
                        $xt_server = $sv;
                        break;
                    }
                }
            }
        }elseif($xt_server->state == "B"){

            //Debug::output("從bwf離開");
            // 先在ops裡面找屬於該pid的連線
            if(isset(self::$ops->plist[$next_depart->pid])){
                $xt_server = self::$ops;
            }else{
                foreach(self::$spf as $sv){
                    if(array_key_exists($next_depart->pid,$sv->plist)){
                        //Debug::output("從spf離開");
                        $xt_server = $sv;
                        $departed = true;
                        break;
                    }
                }
            }

            if(!$departed){
                foreach(self::$full as $sv){
                    if(array_key_exists($next_depart->pid,$sv->plist)){
                        //Debug::output("從full離開");
                        $xt_server = $sv;
                        break;
                    }
                }
            }
        }

        if( $fm_server == $xt_server ){
            
            // 從自身離線
            //Debug::output("從自身離開");
            //Debug::server_state($fm_server);

            // 維護server plist 及bw sp
            //Debug::output("維護server plist 及bw sp");
            if(!isset($fm_server->plist[$next_depart->pid])){
                Debug::error_report("見鬼了 伺服器列表裡面沒有這個pid，卻有連線要離開");
                Debug::conn_state($next_depart);
                exit;
            }
            if(count($fm_server->plist[$next_depart->pid]) == 1){
                //Debug::output("此pid只剩一個");
                //Debug::output("清除plist 取回頻寬與空間");
                unset($fm_server->plist[$next_depart->pid]);
                //Debug::output("a刪除sid: $fm_server->sid 的pid: $next_depart->pid");
                $fm_server->space        += $next_depart->space;
                $fm_server->bandwidth    += $next_depart->bandwidth;
            }else{
                //Debug::output("此pid伺服器內還有剩");
                $fm_server->bandwidth    += $next_depart->bandwidth;
                unset($fm_server->plist[$next_depart->pid][$next_depart->cnid]);
            }

            // 刪除連線
            if(isset(self::$conn[$next_depart->cnid])){
                //Debug::output("刪除連線");
                //Debug::conn_state($next_depart);
                unset(self::$conn[$next_depart->cnid]);
                self::$NUM_CONN --;
            }else{
                Debug::error_report("cnid: $next_depart->sid");
                Debug::error_report("這條連線還沒刪除就不見了");
                exit;
            }
            if($fm_server->space > 0 and $fm_server->bandwidth > 0 and $fm_server->state != 'O'){
                //Debug::output("merge fm_server");
                $this->merge($fm_server);
                //Debug::output("merge 結束後的fm_server");
                //Debug::server_state($fm_server);
            }
        }else{
            
            // 從其他伺服器離線
            // 將連線從xt_server移出來
            //Debug::output("將連線從xt_server移出來");
            //Debug::server_state($xt_server);
            $td_cnid = array_pop($xt_server->plist[$next_depart->pid]);
            /*
            if($td_cnid == null){
                echo "要離開的pid: $next_depart->pid\n";
                var_dump($td_cnid);
                var_dump($xt_server);
                exit;
            }
            */
            //Debug::conn_state(self::$conn[$td_cnid]);

            // 整理 xt_server bw sp
            if(count($xt_server->plist[$next_depart->pid]) == 0){

                unset($xt_server->plist[$next_depart->pid]);
                //Debug::output("b刪除sid: $xt_server->sid 的pid: $next_depart->pid");
                $xt_server->space        += $next_depart->space;
                $xt_server->bandwidth    += $next_depart->bandwidth;
                //Debug::output("xt_server 釋出空間");

            }else{
                $xt_server->bandwidth    += $next_depart->bandwidth;
            }

            // 清除plist記錄
            unset($fm_server->plist[$next_depart->pid][$next_depart->cnid]);

            // 將連線加入fm_server
            $fm_server->plist[$next_depart->pid][$td_cnid] = $td_cnid;
            self::$conn[$td_cnid]->sid = $fm_server->sid;

            // 刪除next_depart
            //Debug::output("刪除連線");
            //Debug::conn_state($next_depart);
            unset(self::$conn[$next_depart->cnid]);
            self::$NUM_CONN --;
            if($xt_server->space > 0 and $xt_server->bandwidth > 0 and $xt_server->state != 'O'){
                //Debug::output("merge xt_server");
                $this->merge($xt_server);
            }
        }
        //Debug::output("depart 後fm");
        //Debug::server_state($fm_server);
        //Debug::output("depart 後xt");
        //Debug::server_state($xt_server);
        /*
        if(isset(self::$ops)){
            //Debug::output("depart 後ops");
            //Debug::server_state(self::$ops);
        }
        if($xt_server->state == 'S'){
            if($xt_server->space > 0){
                Debug::error_report("space 錯誤");
                //Debug::server_state($xt_server);
                exit;
            }
        }
        if($fm_server->state == 'S'){
            if($fm_server->space > 0){
                Debug::error_report("space 錯誤");
                //Debug::server_state($xt_server);
                exit;
            }
        }
        */

    }

    private function merge($sv){
        
        /* 
        *   1.移動相同pid的連線
        *   2.移動相異pid的連線
        */
        switch($sv->state){
            case 'B':
                unset(self::$bwf[$sv->sid]);
                break;
            case 'S':
                unset(self::$spf[$sv->sid]);
                break;
            case 'F':
                unset(self::$full[$sv->sid]);
                break;
        }
        
        if(self::$ops and $sv->sid == self::$ops->sid){
           // Debug::output("merge ops");
           // Debug::server_state($sv);
           // Debug::server_state(self::$ops);
            $sv->state = 'O';
            return;
        }

        //Debug::output("merge");
        if(self::$ops == null){
            if($sv->bandwidth > 0 and $sv->space > 0){
                //Debug::server_state($sv);
                self::$ops = $sv;
                self::$ops->state = 'O';
                return;
            }else{
                exit;
            }
        }
        /*
        elseif(self::$ops->bandwidth > BW_PER_SERVER){
            Debug::error_report("ops error");
            //Debug::server_state(self::$ops);
            exit;
        }
        //Debug::output("merge修改伺服器頻寬前");
        //Debug::server_state($sv);
        //Debug::server_state(self::$ops);
        if($sv->space < 0 or $sv->bandwidth < 0)
            exit;

        if($sv->sid == self::$ops->sid){
            Debug::error_report("ops 不應該丟到merge裡面", self::$ops);
            exit;
        }
        if($sv->bandwidth < 0)
            exit;
        */

        $bwf = false;

        if(self::$ops){
            
            // 先移動相同pid的連線到sv
            //Debug::output("先移動相同pid的連線到sv");

            // 取得相同pid陣列
            //Debug::output("取得相同pid陣列");
            while($intersect = array_intersect(array_keys($sv->plist),array_keys(self::$ops->plist))){

                // 針對每個pid移動旗下的conn
                //Debug::output("針對每個pid移動旗下的conn");
                foreach($intersect as $pid){

                    $ops_cnid = array_keys(self::$ops->plist[$pid]);
                    $c_count = count($ops_cnid);
                    $sv_bw = $sv->bandwidth / Env::$BW_PER_PROGRAM;

                    if( $sv_bw >= $c_count ){

                        // 移動ops中此pid底下的所有conn到sv
                        //Debug::output("移動ops中此pid底下的所有conn到sv");

                        // 修改sid 以及plist
                        foreach($ops_cnid as $cnid){

                            if(!isset(self::$conn[$cnid])){
                                Debug::output("在conn中找不到$cnid",self::$ops->plist[$pid][$cnid]);
                                exit;
                            }
                           // Debug::output("conn 的sid 原本是" . self::$conn[$cnid]->sid);
                           // Debug::conn_state(self::$conn[$cnid]);
                            self::$conn[$cnid]->sid = $sv->sid;
                           // Debug::output("修改後變成" . self::$conn[$cnid]->sid);
                           // Debug::conn_state(self::$conn[$cnid]);

                            $sv->plist[$pid][$cnid] = $cnid;

                            // 修改伺服器頻寬
                            $sv->bandwidth -= Env::$BW_PER_PROGRAM;
                            self::$ops->bandwidth += Env::$BW_PER_PROGRAM;
                        }
                        unset(self::$ops->plist[$pid]);
                        //Debug::output("c刪除sid: ".self::$ops->sid." 的pid: $pid");

                        self::$ops->space += Env::$SPACE_PER_PROGRAM;
                        //Debug::output("修改伺服器頻寬後");
                        //Debug::output("sv");
                        //Debug::server_state($sv);
                        //Debug::output("ops");
                        //Debug::server_state(self::$ops);
                    }else{

                        // 移動部分ops中pid旗下的conn
                        // 結束後sv的頻寬將被填滿
                        //Debug::output("移動部分ops中pid旗下的conn");
                        //Debug::output("結束後sv的頻寬將被填滿");

                        //Debug::output("修改伺服器頻寬前");
                        //Debug::output("sv");
                        //Debug::server_state($sv);
                        //Debug::output("ops");
                        //Debug::server_state(self::$ops);
                        // 修改sid 以及plist
                        for( $i = 0; $i < $sv_bw; $i++){
                            $cnid = next($ops_cnid);
                            //Debug::output("conn 的sid 原本是" . self::$conn[$cnid]->sid);
                            //Debug::conn_state(self::$conn[$cnid]);
                            
                            self::$conn[$cnid]->sid = $sv->sid;

                            $sv->plist[$pid][$cnid] = $cnid;
                            unset(self::$ops->plist[$pid][$cnid]);
                            //Debug::output("修改後變成" . self::$conn[$cnid]->sid);
                            //Debug::conn_state(self::$conn[$cnid]);

                            // 修改伺服器頻寬
                            $sv->bandwidth -= Env::$BW_PER_PROGRAM;
                            self::$ops->bandwidth += Env::$BW_PER_PROGRAM;
                        }
                        //Debug::output("修改伺服器頻寬後");
                        //Debug::output("sv");
                        //Debug::server_state($sv);
                        //Debug::output("ops");
                        //Debug::server_state(self::$ops);
                        $bwf = true;
                        break;
                    }
                }

                if($bwf){
                    break;
                }
            }

            // 檢查兩伺服器狀態
            //Debug::output("檢查兩伺服器狀態");

            if($sv->bandwidth == 0){
                $sv->state = 'B';
                self::$bwf[$sv->sid] = $sv;
                if(self::$ops->space == Env::$SPACE_PER_SERVER and self::$ops->bandwidth == Env::$BW_PER_SERVER){
                    unset(self::$servers[self::$ops->sid]);
                    self::$ops = null;
                }
                return;
            }elseif(self::$ops->space == Env::$SPACE_PER_SERVER and self::$ops->bandwidth == Env::$BW_PER_SERVER){
                unset(self::$servers[self::$ops->sid]);
                self::$ops = $sv;
                self::$ops->state = 'O';
                return;
            }

            // 移動相異pid的連線到sv
            //Debug::output("移動相異pid的連線到sv");

            $spf = false;
            
            //Debug::server_state(self::$ops);
            foreach( array_keys(self::$ops->plist) as $pid ){
                
                $ops_cnid = array_keys(self::$ops->plist[$pid]);
                $c_count = count($ops_cnid);
                $sv_bw = $sv->bandwidth / Env::$BW_PER_PROGRAM;

                if( $sv_bw >= $c_count ){

                    // 移動ops中此pid底下的所有conn到sv

                    //Debug::output("移動sid:" . self::$ops->sid . "中此$pid 底下的所有conn到sid:$sv->sid");
                    //Debug::output("修改前伺服器頻寬");
                    //Debug::output("sv");
                    //Debug::server_state($sv);
                    //Debug::output("ops");
                    //Debug::server_state(self::$ops);

                    // 修改sid 以及plist
                    foreach($ops_cnid as $cnid){

                            //Debug::output("conn 的sid 原本是" . self::$conn[$cnid]->sid);
                            //Debug::conn_state(self::$conn[$cnid]);
                        self::$conn[$cnid]->sid = $sv->sid;

                            //Debug::output("修改後變成" . self::$conn[$cnid]->sid);
                            //Debug::conn_state(self::$conn[$cnid]);
                        $sv->plist[$pid][$cnid] = $cnid;
                        $sv->bandwidth -= Env::$BW_PER_PROGRAM;
                        self::$ops->bandwidth += Env::$BW_PER_PROGRAM;
                    }
                    unset(self::$ops->plist[$pid]);
                    //Debug::output("d刪除sid: ".self::$ops->sid." 的pid: $pid");

                    // 修改伺服器頻寬
                    //Debug::output("修改後伺服器頻寬478");
                    $sv->space -= Env::$SPACE_PER_PROGRAM;
                    self::$file_add ++;
                    self::$ops->space += Env::$SPACE_PER_PROGRAM;
                    //Debug::output("sv");
                    //Debug::server_state($sv);
                    //Debug::output("ops");
                    //Debug::server_state(self::$ops);
                    if($sv->space == 0){
                        $sv->state = 'S';
                        $spf = true;
                        break;
                    }
                    if($sv->bandwidth == 0){
                        $sv->state = 'B';
                        $bwf = true;
                        break;
                    }
                }else{

                    // 移動部分ops中pid旗下的conn
                    // 結束後sv的頻寬將被填滿

                    //Debug::output("移動sid:" . self::$ops->sid . "中此$pid 底下的部分conn到sid:$sv->sid");

                    // 修改sid 以及plist
                    for( $i = 0; $i < $sv_bw; $i++){
                        $cnid = next($ops_cnid);
                            //Debug::output("conn 的sid 原本是" . self::$conn[$cnid]->sid);
                            //Debug::conn_state(self::$conn[$cnid]);
                        
                        self::$conn[$cnid]->sid = $sv->sid;

                            //Debug::output("修改後變成" . self::$conn[$cnid]->sid);
                            //Debug::conn_state(self::$conn[$cnid]);
                        $sv->plist[$pid][$cnid] = $cnid;
                        unset(self::$ops->plist[$pid][$cnid]);
                        self::$ops->bandwidth += Env::$BW_PER_PROGRAM;
                    }
                    // 修改伺服器頻寬
                    $sv->bandwidth = 0;
                    $sv->space -= Env::$SPACE_PER_PROGRAM;
                    self::$file_add ++;
                    $bwf = true;
                    if($sv->space == 0){
                        $spf = true;
                    }
                    break;
                }
            }
            //Debug::server_state(self::$ops);

            if($sv->space == 0 and $sv->bandwidth == 0){
                $sv->state = 'F';
                self::$full[$sv->sid] = $sv;
                if(self::$ops->space == Env::$SPACE_PER_SERVER and self::$ops->bandwidth == Env::$BW_PER_SERVER){
                    unset(self::$servers[self::$ops->sid]);
                    self::$ops = null;
                }
                return;
            }elseif($sv->space == 0 and $sv->bandwidth > 0){
                $sv->state = 'S';
                self::$spf[$sv->sid] = $sv;
                if(self::$ops->space == Env::$SPACE_PER_SERVER and self::$ops->bandwidth == Env::$BW_PER_SERVER){
                    unset(self::$servers[self::$ops->sid]);
                    self::$ops = null;
                }
                return;
            }elseif($sv->space > 0 and $sv->bandwidth == 0){
                $sv->state = 'B';
                self::$bwf[$sv->sid] = $sv;
                if(self::$ops->space == Env::$SPACE_PER_SERVER and self::$ops->bandwidth == Env::$BW_PER_SERVER){
                    unset(self::$servers[self::$ops->sid]);
                    self::$ops = null;
                }
                return;
            }elseif(self::$ops->space == Env::$SPACE_PER_SERVER and self::$ops->bandwidth == Env::$BW_PER_SERVER){
                unset(self::$servers[self::$ops->sid]);
                self::$ops = $sv;
            }
        }else{
            self::$ops = $sv;
            $sv->state = 'O';
            return;
        }
    }

    /*
    *   把bwf與spf聯集的連線移動到spf
    */
    public function stage_1($spf_sid_list = null){

        $again = null;
        
        $count_spf = count(Proc::$spf);
        if($count_spf  == 0){
            return false;
        }

        //Debug::output("spf_merge start");

        if($count_spf > 1){

            $spf_key = array_keys(Proc::$spf);
            for($i = 0; $i < $count_spf-1; $i++){
                for($j = $count_spf - 1; $j > $i; $j--){
                    $sid = $spf_key[$i];
                    $sjd = $spf_key[$j];
                    if($intersect = array_intersect( array_keys( Proc::$spf[$sid]->plist ), array_keys( Proc::$spf[$sjd]->plist ) ) ){
                        foreach( $intersect as $pid){

                            $ops_cnid = array_keys(self::$spf[$sjd]->plist[$pid]);
                            $c_count = count($ops_cnid);
                            $sv_bw = self::$spf[$sid]->bandwidth / Env::$BW_PER_PROGRAM;
                            if( $sv_bw >= $c_count ){
                                foreach($ops_cnid as $cnid){
                                    
                                    self::$conn[$cnid]->sid = $sid;
                                    self::$spf[$sid]->plist[$pid][$cnid] = $cnid;
                                    self::$spf[$sid]->bandwidth -= Env::$BW_PER_PROGRAM;
                                    self::$spf[$sjd]->bandwidth += Env::$BW_PER_PROGRAM;
                                }
                                //Debug::output("e刪除sid: ".$sjd." 的pid: $pid");
                                unset(self::$spf[$sjd]->plist[$pid]);
                                self::$spf[$sjd]->space += Env::$SPACE_PER_PROGRAM;
                                $this->merge(self::$spf[$sjd]);
                            }else{
                                for($k = 0; $k < $sv_bw; $k++){

                                    $cnid = next($ops_cnid);
                                    self::$conn[$cnid]->sid = $sid;
                                    self::$spf[$sid]->plist[$pid][$cnid] = $cnid;
                                    self::$spf[$sid]->bandwidth -= Env::$BW_PER_PROGRAM;
                                    unset(self::$spf[$sjd]->plist[$pid][$cnid]);
                                    self::$spf[$sjd]->bandwidth += Env::$BW_PER_PROGRAM;
                                }
                                self::$spf[$sid]->state = 'F';
                                self::$full[$sid] = self::$spf[$sid];
                                unset(self::$spf[$sid]);
                                break;
                            }
                        }
                    }
                    else{
                        //Debug::output("sid: $sid 與 sid: $sjd 找不到共同的pid");
                        //Debug::output("server $sid",array_keys(Proc::$spf[$sid]->plist));
                        //Debug::output("server $sid",array_keys(Proc::$spf[$sjd]->plist));
                    }
                    if(!isset(self::$spf[$sid])){
                        break;
                    }
                }
            }
        }else{
            //Debug::output("SPF 數量太少");
            return;
        }
        //Debug::output("spf_merge end");

        // 將 bwf 與 spf 共同的連線移動到 spf
        $count_s = count(self::$spf);
        if($count_s > 0){
            $spf_key = array_keys(Proc::$spf);
            for($j = 0; $j < $count_s; $j++){
                $sid = $spf_key[$j];
                foreach(self::$bwf as $bf){
                    if(!isset(self::$spf[$sid])){
                        Debug::error_report("找不到這個sid",$sid);
                        Debug::error_report("spf keys",$spf_key);
                        Debug::error_report("系統上的spf sid",array_keys(self::$spf));
                        exit;
                    }
                    if($intersect = array_intersect(array_keys(self::$spf[$sid]->plist),array_keys($bf->plist))){
                        foreach($intersect as $pid){
                            if(!isset($bf->plist[$pid])){
                                Debug::error_report("bwf sid",$bf->sid);
                                Debug::error_report("找不到這個pid",$pid);
                                Debug::error_report("bwf pid list",array_keys($bf->plist));
                                exit;
                            }else{
                                Debug::error_report("bwf sid",$bf->sid);
                            }
                            $bf_cnid = array_keys($bf->plist[$pid]);
                            $c_count = count($bf_cnid);
                            $sv_bw = self::$spf[$sid]->bandwidth / Env::$BW_PER_PROGRAM;
                            if( $sv_bw >= $c_count ){
                                Debug::output("從 $bf->sid 移除 $c_count 個連線");
                                Debug::output("移除前");
                                Debug::server_state($bf);
                                foreach($bf_cnid as $cnid){
                                    
                                    self::$conn[$cnid]->sid = $sid;
                                    self::$spf[$sid]->plist[$pid][$cnid] = $cnid;
                                    self::$spf[$sid]->bandwidth -= Env::$BW_PER_PROGRAM;
                                    $bf->bandwidth += Env::$BW_PER_PROGRAM;
                                }
                                Debug::output("f刪除sid: ".$bf->sid." 的pid: $pid");
                                unset($bf->plist[$pid]);
                                $bf->space += Env::$SPACE_PER_PROGRAM;
                                Debug::server_state($bf);
                            }else{
                                for($k = 0; $k < $sv_bw; $k++){

                                    $cnid = next($bf_cnid);
                                    self::$conn[$cnid]->sid = $sid;
                                    self::$spf[$sid]->plist[$pid][$cnid] = $cnid;
                                    self::$spf[$sid]->bandwidth -= Env::$BW_PER_PROGRAM;
                                    unset($bf->plist[$pid][$cnid]);
                                    $bf->bandwidth += Env::$BW_PER_PROGRAM;
                                }
                                Debug::server_state(self::$spf[$sid]);
                                self::$spf[$sid]->state = 'F';
                                self::$full[$sid] = self::$spf[$sid];
                                unset(self::$spf[$sid]);
                                $this->merge($bf);
                                break;
                            }
                        }
                    }
                    if(!isset(self::$spf[$sid])){
                        break;
                    }
                }
            }
        }else{
            return;
        }

        //Debug::output("end stage_1");
        if($again){
            $this->stage_1($again);
        }
    }

    /*
    *   整理m-bwf中頻寬沒滿的伺服器
    *   m-bwf與ops merge
    */

    public function stage_3(){
        reset(self::$bwf);
        if(count(self::$spf) > 0){
            foreach( self::$spf as $sv ){
                $bwf = current(self::$bwf);
                
                // sort pid by count asc
                
                $spf_pids = array();
                foreach( $sv->plist as $pid => $value ){
                    if(!isset($spf_pids[$pid]))
                        $spf_pids[$pid] = count($value);
                }

                asort($spf_pids);

                $bwf_pids = array();
                if(!is_object($bwf)){
                    Debug::error_report("bwf error",$bwf);
                    Debug::server_state($bwf);
                    exit;
                }
                foreach( $bwf->plist as $pid => $value ){
                    if(!isset($bwf_pids[$pid])){
                        $bwf_pids[$pid] = count($value);
//                        echo $pid,count($value),"\n";
                    }
                }

                arsort($bwf_pids);

                while( $sv->bandwidth > 0 ){
                    $sv_pid = key($spf_pids);
                    $bwf_pid = key($bwf_pids);
                    if( $sv->bandwidth / Env::$BW_PER_PROGRAM + $spf_pids[$sv_pid] == $bwf_pids[$bwf_pid]){
                    
                    // 完全交換該pid的連線
                        if(!isset($bwf->plist[$bwf_pid])){
                            Debug::output("2no pid $bwf_pid in $bwf->sid");
                            Debug::output("bwf",$bwf);
                            Debug::output("pids",$bwf_pids);
                            exit;
                        }
                        $this->swap_all_conn($sv, $sv_pid, $bwf, $bwf_pid);

                    }elseif($sv->bandwidth / Env::$BW_PER_PROGRAM + $spf_pids[$sv_pid] < $bwf_pids[$bwf_pid]){
                        
                    // 移動部分連線到spf ，spf 則移動整個pid的連線到bwf

                        // 從spf移動到bwf
                        foreach( $sv->plist[$sv_pid] as $cnid){
                            $osid = self::$conn[$cnid]->sid;
                            self::$conn[$cnid]->sid = $bwf->sid;
                            $bwf->plist[$sv_pid][$cnid] = $cnid;
                            $bwf->bandwidth -= Env::$BW_PER_PROGRAM;
                            $sv->bandwidth  += Env::$BW_PER_PROGRAM;
                            Debug::output("c $cnid 的sid 從$osid 改成$bwf->sid");
                        }
                        Debug::output("g刪除sid: ".$sv->sid." 的pid: $$sv_pid");
                        unset($sv->plist[$sv_pid]);

                        // 從bwf移動部分連線到spf
                        $cnid = current($bwf->plist[$bwf_pid]);
                        for($k = 0; $k < $sv->bandwidth / Env::$BW_PER_PROGRAM; $k++){
                            $osid = self::$conn[$cnid]->sid;
                            self::$conn[$cnid]->sid = $sv->sid;
                            $sv->plist[$bwf_pid][$cnid] = $cnid;
                            $bwf->bandwidth += Env::$BW_PER_PROGRAM;
                            $sv->bandwidth  -= Env::$BW_PER_PROGRAM;
                            Debug::output("d $cnid 的sid 從$osid 改成$sv->sid");
                            $cnid = next($bwf->plist[$bwf_pid]);
                        }
                        $bwf->space -= Env::$SPACE_PER_PROGRAM;
                        
                    }elseif( $spf_pids[$sv_pid] < $bwf_pids[$bwf_pid]){
                        
                    // 完全交換該pid的連線
                        if(!isset($bwf->plist[$bwf_pid])){
                            Debug::output("1no pid $bwf_pid in $bwf->sid");
                            Debug::server_state($bwf);
                            exit;
                        }
                        $this->swap_all_conn($sv, $sv_pid, $bwf, $bwf_pid);

                    }
                    /*
                    elseif($spf_pids[$sv_pid] > $bwf_pids[$bwf_pid]){

                    // 這一塊不應該被執行的
                        Debug::error_report("這一塊不應該被執行的 bwf_pids",$bwf_pids);
                        Debug::error_report("這一塊不應該被執行的 spf_pids",$spf_pids);
                        exit;
                        $bwf = next(self::$bwf);
                        foreach( $bwf->plist as $pid => $value ){
                            if(!isset($bwf_pids[$pid]))
                                $bwf_pids[$pid] = count($value);
                        }
                        asort($bwf_pids);
                    }
                    */
                    next($spf_pids);
                    next($bwf_pids);
                }

                next(self::$bwf);
                $this->merge($bwf);
                $sv->state = 'F';
                self::$full[$sv->sid] = $sv;
                unset(self::$spf[$sv->sid]);
            }
        }else{
            return;
        }
    }

    private function swap_all_conn($sv1,$pid1,$sv2,$pid2){
        
        foreach($sv1->plist[$pid1] as $cnid){
            $osid = self::$conn[$cnid]->sid;
            self::$conn[$cnid]->sid = $sv2->sid;
            $sv2->plist[$pid1][$cnid] = $cnid;
            $sv1->bandwidth += Env::$BW_PER_PROGRAM;
            $sv2->bandwidth -= Env::$BW_PER_PROGRAM;
            Debug::output("a $cnid 的sid 從$osid 改成$sv2->sid");
        }
        foreach($sv2->plist[$pid2] as $cnid){
            $osid = self::$conn[$cnid]->sid;
            self::$conn[$cnid]->sid = $sv1->sid;
            $sv1->plist[$pid2][$cnid] = $cnid;
            $sv1->bandwidth -= Env::$BW_PER_PROGRAM;
            $sv2->bandwidth += Env::$BW_PER_PROGRAM;
            Debug::output("b $cnid 的sid 從$osid 改成$sv1->sid");
        }

        unset($sv1->plist[$pid1]);
        unset($sv2->plist[$pid2]);
    }

    public function stage3(){
    
        $db = Mysql::get_db();
        $stmt = $db->prepare("select sid from server_table where state='S'");
        $stmt->execute();
        $stmt->bindColumn("sid", $sid);
        while($stmt->fetch(PDO::FETCH_BOUND)){
            //$db->query("update server_table set state='O' where sid=$sid");
            $spf_list[] = new Server($sid);
            //Debug::output("sid of spf: $sid");
        }
        $num_spf = count($spf_list);
        if( $num_spf ){
            for( $i = 0; $i < $num_spf; $i++){
                
                $stmt = $db->prepare("select sid from server_table where state='B'");
                $stmt->execute();
                $stmt->bindColumn("sid", $sid);

                while($stmt->fetch(PDO::FETCH_BOUND)){
                    //$db->query("update server_table set state='O' where sid=$sid");
                    $bwf_list[] = new Server($sid);
                    //Debug::output("sid of bwf: $sid");
                }
                $num_bwf = isset($bwf_list) ? count($bwf_list) : null;

                if($num_bwf){
                    for( $j = 0; $j < $num_bwf; $j++){

                        $spf_list[$i]->update_plist("asc");
                        $bwf_list[$j]->update_plist("desc");
                        //Debug::output("spf",$spf_list[$i]->plist['count'][0]);
                        //Debug::output("bwf",$bwf_list[$j]->plist['count'][0]);
                        $bw_ava = $spf_list[$i]->bandwidth / BW_PER_PROGRAM;
                        
                        // Find pid with max connections in bwf
                        $max_bwf['count'] = $bwf_list[$j]->plist['count'][$j];
                        $max_bwf['pid'] = $bwf_list[$j]->plist['pid'][$j];

                        // Find pid with min connections in spf
                        $min_spf['count'] = $spf_list[$i]->plist['count'][$j];
                        $min_spf['pid'] = $spf_list[$i]->plist['pid'][$j];

                        //Debug::output("spf",$min_spf);
                        //Debug::output("bwf",$max_bwf);
                        //Debug::output("bw_ava",$bw_ava);
                        //Debug::output("i{$i} j{$j}");

                        if($bw_ava + $min_spf['count'] == $max_bwf['count']){

                        // 兩邊完全互換
                            //Debug::output("兩邊完全互換 spf 變成full");
                            $c_to_move = $max_bwf['count'];
                        }elseif($bw_ava + $min_spf['count'] < $max_bwf['count']){

                        // spf的完全移到bwf, bwf 移動部分連線過去spf
                            $c_to_move = $bw_ava + $min_spf['count'];
                            //Debug::output("spf的完全移到bwf, bwf 移動{$c_to_move}連線過去spf");
                        }elseif($min_spf['count'] < $max_bwf['count']){
                        
                        // 兩邊完全互換
                            $c_to_move = $max_bwf['count'];
                            //Debug::output("兩邊完全互換");
                        }elseif($bw_ava >= $max_bwf['count']){
                            
                        // 換下一個bwf
                            //Debug::output("換下一個bwf");
                            break;
                        }

                        $this->swap_conn($bwf_list[$j],$max_bwf['pid'],$c_to_move,$spf_list[$i],$min_spf['pid'],$min_spf['count']);
                        //$bwf_list[$j]->update_state();
                        $spf_list[$i]->update_state();
                        $bwf_list[$j]->merge();
                        if($bwf_list[$j]->state == 'S'){
                            $this->stage_1(array($bwf_list[$j]->sid));
                        }
                    }
                }else{
                    // no bwf exist
                    //Debug::output("完全沒有bwf");
                    return;
                }
            }
        }else{
            // no spf exist
            //Debug::output("完全沒有spf");
            return;
        }
    }
    
}
