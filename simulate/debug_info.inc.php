<?php
// Last Update:2013/09/12 01:42:37 

class Debug {

    public static function output($message,$data = null){
    
        echo $message . "\n";
        if(isset($data)){
            var_dump($data);
        }
    }

    public static function error_report($message,$data = null){
    
        echo $message . "\n";
        if(isset($data)){
            var_dump($data);
        }
    }

    public static function conn_state($conn){
        echo "cnid: $conn->cnid, sid: $conn->sid, pid: $conn->pid, arrivale: $conn->arrival, depart: $conn->depart \n";
    }

    public static function server_state($sv){
        $count = 0;
        if(!is_object($sv)){
            self::error_report("出現空的伺服器, error");
            exit;
        }
        foreach($sv->plist as $pid){
            $count += count($pid);
        }
        foreach(Proc::$bwf as $bwf_sv){
            if($bwf_sv->state != 'B'){
                self::error_report("BWF Error",$bwf_sv);
                exit;
            }
        }
        foreach(Proc::$spf as $spf_sv){
            if($spf_sv->state != 'S'){
                self::error_report("spf Error",$spf_sv);
                exit;
            }
        }
        foreach(Proc::$full as $full_sv){
            if($full_sv->state != 'F'){
                self::error_report("full Error",$full_sv);
                exit;
            }
        }
        $bwf_count = count(Proc::$bwf);
        $spf_count = count(Proc::$spf);
        $full_count = count(Proc::$full);
        $ops_count = count(Proc::$ops);
        $total = count(Proc::$servers);
        echo "Sid:",$sv->sid,"\tbandwidth:",(($sv->bandwidth ) / Env::$BW_PER_PROGRAM),"\tspace:",( (Env::$SPACE_PER_SERVER - $sv->space) / Env::$SPACE_PER_PROGRAM),"\tconn_count:" , $count ,"\tpid_count:",count($sv->plist),"\tstate:",$sv->state, "\n";
        self::error_report("full:$full_count bwf:$bwf_count spf:$spf_count ops:$ops_count total:$total");
        foreach(Proc::$servers as $sid=>$svv){
            self::error_report("sid:$sid state:$svv->state");
        }
        self::error_report("bwf:",array_keys(Proc::$bwf));
        self::error_report("spf:",array_keys(Proc::$spf));
        self::error_report("full:",array_keys(Proc::$full));
        self::error_report("ops:",(Proc::$ops->sid));
        if($count * Env::$BW_PER_PROGRAM + $sv->bandwidth != Env::$BW_PER_SERVER or $count * Env::$BW_PER_PROGRAM > Env::$BW_PER_SERVER){
            self::error_report("bw error");
            self::error_report("cnid count: $count");
            self::error_report("server bw: $sv->bandwidth");
            echo "Sid:",$sv->sid,"\tbandwidth:",(($sv->bandwidth ) / Env::$BW_PER_PROGRAM),"\tspace:",( (Env::$SPACE_PER_SERVER - $sv->space) / Env::$SPACE_PER_PROGRAM),"\tconn_count:" , $count ,"\tpid_count:",count($sv->plist),"\tstate:",$sv->state, "\n";
            exit;
        }
        if(count($sv->plist)*Env::$SPACE_PER_PROGRAM + $sv->space != Env::$SPACE_PER_SERVER or count($sv->plist) * Env::$SPACE_PER_PROGRAM > Env::$SPACE_PER_SERVER){
            self::error_report("space error");
            exit;
        }
        if(count(Proc::$bwf) + count(Proc::$spf) + count(Proc::$ops) + count(Proc::$full) != count(Proc::$servers)){
            self::error_report("server counts error full:$full_count bwf:$bwf_count spf:$spf_count total:$total",Proc::$ops);
            exit;
        }
    }

    public static function cur_state($flag, $sys_time, $eplased){
        $left = ( 86400 - $sys_time ) * $eplased / $sys_time;
        echo "\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\r";
        printf( "%s %5.2f %3d:%2d:%2d  %5.1fx  預估剩下%3d天%3d小時\t%2.2f%%\r", $flag, $sys_time, $eplased / 60 / 60,$eplased / 60 % 60, $eplased % 60, ( $eplased / $sys_time ) , ( $left /60 /60 /24 ), ( $left / 60 / 60 ) % 24, ( $sys_time / 864 ) );
    }

    public static function func_start($func_name){
        
        return;
        echo "$func_name\tstart\n";
    }
    public static function func_end($func_name){
        
        return;
        echo "$func_name\tend\n";
    }
}
