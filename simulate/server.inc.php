<?php
// Last Update:2013/08/29 03:52:47 

class Server {

    public $sid = null;
    public $space = null;
    public $bandwidth = null;
    public $state = null;
    public $plist = array();
    public static $is_merge = false;
    public static $count_spf = 0;

    public function __call( $name, $arguments){
    
        echo "注意! 注意! class Server 中 有個叫做'$name' 的方法還沒寫\n"
                     . implode(', ', $arguments). "\n";
    }

    public function __construct( $sid = null ){
        if($sid){
            return Proc::$servers[$sid];
        }else{
            if(Proc::$ops == null){
                // Can not find OPS, then give one
                $this->add_new_server();
            }
            else{
                return Proc::$ops;
            }
        }
    }

    private function add_new_server(){
        
        $this->sid = Proc::$sid++;
        $this->state = 'O';
        $this->bandwidth = Env::$BW_PER_SERVER;
        $this->space = Env::$SPACE_PER_SERVER;
        Proc::$servers[$this->sid] = $this;
    }
}
