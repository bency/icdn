<?php
// Last Update:2013/09/15 12:29:52 
include dirname ( __FILE__ ).'/constant.inc.php';
$env = new Env($argv[1]);
$host = 'localhost';
$setting = parse_ini_file("connect.ini");
$user = $setting['username'];
$passwd = $setting['password'];
//$hash = exec("git log --pretty=format:'%h' -n 1");
$database = Env::$DATABASE;

$db = new mysqli ( $host,$user,$passwd );

if ( $db->connect_errno ){
    echo "Failed to connect to MySQL: ( " . $db->connect_errno . " ) " . $db->connect_error . "\n";
}
else{
    echo "Connecting to MySQL success.\n";
}

if ( !$db->set_charset("utf8")){
	echo "Set charset error $db->error\n";
}

if( !$db->query ( "DROP DATABASE IF EXISTS $database" ) || !$db->query( "CREATE DATABASE $database" ) ) {
        echo "Database creation failed: ( " . $db->errno . " ) " . $db->error . "\n";
}
else{
    echo "Database $database created successful.\n";
}

if ( !$db->query ( "DROP TABLE IF EXISTS $database.delete_table" ) ||
    !$db->query ( "
        CREATE TABLE $database.delete_table (
        cnid INT NOT NULL,
        pid  INT NOT NULL,
        sid  INT NOT NULL,
        arrival DOUBLE NOT NULL,
        depart  DOUBLE NOT NULL,
        bandwidth INT NOT NULL,
        space INT NOT NULL)DEFAULT CHARSET=utf8" )){
    echo "Table delete_table creation failed: ( " . $db->errno . " ) " . $db->error . "\n";
}
else{
    echo "Table delete_table created successful.\n";
}

if ( !$db->query ( "DROP TABLE IF EXISTS $database.log_table" ) or
    !$db->query ( "
        CREATE TABLE $database.log_table (
        sys_time INT NOT NULL AUTO_INCREMENT,
        bw_conn  INT NOT NULL DEFAULT 0,
        bw_sv    INT NOT NULL DEFAULT 0,
        num_sv_total INT NOT NULL DEFAULT 0,
        num_sv_b INT NOT NULL DEFAULT 0,
        num_sv_s INT NOT NULL DEFAULT 0,
        num_sv_f INT NOT NULL DEFAULT 0,
        num_conn_total INT NOT NULL DEFAULT 0,
        num_pid_total INT NOT NULL DEFAULT 0,
        file_add INT NOT NULL DEFAULT 0,
        PRIMARY KEY ( sys_time )
        )DEFAULT CHARSET=utf8" ) ){
    echo "Table log_table creation failed: ( " . $db->errno . " ) " . $db->error . "\n";
}
else{
    echo "Table log_table created successful.\n";
}

if ( !$db->query( "DROP TABLE IF EXISTS $database.system_table" ) or
     !$db->query( "
        CREATE TABLE $database.system_table (
        setting CHAR(20) NOT NULL,
        value INT NOT NULL)DEFAULT CHARSET=utf8
     " )){
    echo "Table system_table creation failed: ( " . $db->errno . " ) " . $db->error . "\n";
}
else{
    echo "Table system_table created successful.\n";
}

if ( !$db->query("INSERT INTO $database.system_table (setting,value) 
    values('" . 'BW_PER_SERVER' . "','" . Env::$BW_PER_SERVER ."'),
    ('" . 'SPACE_PER_SERVER' ."','" . Env::$SPACE_PER_SERVER ."'),
    ('" . 'BW_PER_PROGRAM' ."','" . Env::$BW_PER_PROGRAM."'),
    ('" . 'SPACE_PER_PROGRAM' ."','" . Env::$SPACE_PER_PROGRAM ."'),
    ('" . 'NUM_OF_PROGRAM' ."','" . Env::$NUM_OF_PROGRAM."'),
    ('" . 'ZIPF' ."','" . Env::$ZIPF."'),
    ('" . 'CONNECT_LIFE' ."','" . Env::$CONNECT_LIFE."'),
    ('" . 'REFRESH_PERIOD' ."','" . Env::$REFRESH_PERIOD."'),
    ('" . 'ARRIVAL_MUL' ."','" . Env::$ARRIVAL_MUL."'),
    ('" . 'TIME_START' ."','" . time() ."')
    ") ){
    echo "Insert system setup data failed: ( " . $db->errno . " ) " . $db->error . "\n";
}
else{
    echo "Insert system setup data successful.\n";
}

if ( !$db->query("DROP TABLE IF EXISTS $database.server_table") or
     !$db->query("Create Table $database.server_table(
     sys_time INT NOT NULL,
     sid INT NOT NULL,
     state char(1) NOT NULL,
     content text NOT NULL
     )DEFAULT CHARSET=utf8")
){
    echo "Table server_table created failed.\n";
}else{
    echo "Table server_table created successful.\n";
}
