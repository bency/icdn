<?php
// Last Update:2013/08/13 15:48:07 
include dirname(__FILE__).'/constant.inc.php';
include dirname(__FILE__).'/mysql.inc.php';
include dirname(__FILE__).'/function.inc.php';

$qt = arrival_in_minute( 20 );
$sum = 0;
for ( $i = 0; $i < $qt; $i ++ ){
    $tmp[0] = $i;
    $tmp[1] = exp_dist( $qt/60 );
    $sum += $tmp[1];
    $dump2[] = $tmp;
}
$sum = 0;
for ( $i = 0; $i < $qt; $i ++ ){
    $tmp[0] = $i;
    $sum += exp_dist( $qt/60 );
    $tmp[1] = $sum;
    $dump[] = $tmp;
}

//echo json_encode($dump) ;
?>
<html>
<head>
<script type="text/javascript" src="https://www.google.com/jsapi"></script>
<script type="text/javascript">
google.load("visualization", "1", {packages:["corechart"]});
google.setOnLoadCallback(drawChart);
function drawChart() {
    var data = google.visualization.arrayToDataTable(<?php echo json_encode($dump)?>);
    var data2 = google.visualization.arrayToDataTable(<?php echo json_encode($dump2)?>);

    var options = {
title: 'Age vs. Weight comparison',
       hAxis: {title: 'times', minValue: 0, maxValue: 15},
       vAxis: {title: 'inter-arrival time', minValue: 0, maxValue: 15},
       legend: 'none'
    };

    var options2 = {
title: 'Age vs. Weight comparison',
       hAxis: {title: 'times', minValue: 0, maxValue: 15},
       vAxis: {title: 'inter-arrival time', minValue: 0, maxValue: 15},
       legend: 'none'
    };

    var chart = new google.visualization.LineChart(document.getElementById('chart_div'));
    var chart2 = new google.visualization.ScatterChart(document.getElementById('chart_div2'));
    chart.draw(data, options);
    chart2.draw(data2, options);
}
</script>
</head>
<body>
<?php
echo "qt:" . $qt . "<br>\n";
echo "lamda:" . ($qt / 60) . "<br>\n";
echo "sum:" . $sum . "<br>\n";
echo "average:" . ( ($sum / $qt) ) . "<br>\n";
echo "1 / average:" . ( 1 / ($sum / $qt) ) . "<br>\n";
?>
<div id="chart_div" style="width: 1350px; height: 500px;"></div>
<div id="chart_div2" style="width: 900px; height: 500px;"></div>
</body>
</html>
