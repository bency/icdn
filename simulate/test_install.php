<?php
// Last Update:2013/08/13 15:47:48 
include dirname ( __FILE__ ).'/constant.inc.php';
$host = 'localhost';
$user = 'root';
$passwd = 'ddyoinejjmj';
$database = 'CDN';

$db = new mysqli ( $host,$user,$passwd );

if ( $db->connect_errno ){
    echo "Failed to connect to MySQL: ( " . $db->connect_errno . " ) " . $db->connect_error . "\n";
}
else{
    echo "Connecting to MySQL success.\n";
}

if ( !$db->query ( "DROP DATABASE IF EXISTS $database" ) ||
    !$db->query ( "CREATE DATABASE $database" ) ) {
        echo "Database creation failed: ( " . $db->errno . " ) " . $db->error . "\n";
}
else{
    echo "Database created successful.\n";
}

if ( !$db->query ( "DROP TABLE IF EXISTS $database.delete_table" ) ||
    !$db->query ( "
        CREATE TABLE $database.delete_table (
        cnid INT NOT NULL AUTO_INCREMENT,
        pid  INT NOT NULL,
        sid  INT NOT NULL,
        arrival DOUBLE NOT NULL,
        depart  DOUBLE NOT NULL,
        bandwidth INT NOT NULL DEFAULT '".BW_PER_PROGRAM."',
        space INT NOT NULL DEFAULT '".SPACE_PER_PROGRAM."',
        PRIMARY KEY ( cnid ))" )){
    echo "Table delete_table creation failed: ( " . $db->errno . " ) " . $db->error . "\n";
}
else{
    echo "Table connect_table created successful.\n";
}

if ( !$db->query ( "DROP TABLE IF EXISTS $database.connect_table" ) ||
    !$db->query ( "
        CREATE TABLE $database.connect_table (
        cnid INT NOT NULL AUTO_INCREMENT,
        pid  INT NOT NULL,
        sid  INT NOT NULL,
        arrival DOUBLE NOT NULL,
        depart  DOUBLE NOT NULL,
        bandwidth INT NOT NULL DEFAULT '".BW_PER_PROGRAM."',
        space INT NOT NULL DEFAULT '".SPACE_PER_PROGRAM."',
        act TINYINT ( 1 ) NOT NULL default 1,
        PRIMARY KEY ( cnid ))" )){
    echo "Table connect_table creation failed: ( " . $db->errno . " ) " . $db->error . "\n";
}
else{
    echo "Table connect_table created successful.\n";
}

if ( !$db->query ( "DROP TABLE IF EXISTS $database.server_table" ) or
    !$db->query ( "
        CREATE TABLE $database.server_table (
        sid INT NOT NULL AUTO_INCREMENT,
        state CHAR ( 1 ) NOT NULL DEFAULT 'O',
        bandwidth INT NOT NULL DEFAULT '".BW_PER_SERVER."',
        space INT NOT NULL DEFAULT '".SPACE_PER_SERVER."',
        PRIMARY KEY ( sid ))" ) ){
    echo "Table server_table creation failed: ( " . $db->errno . " ) " . $db->error . "\n";
}
else{
    echo "Table server_table created successful.\n";
}

if ( !$db->query ( "DROP TABLE IF EXISTS $database.log_table" ) or
    !$db->query ( "
        CREATE TABLE $database.log_table (
        sys_time INT NOT NULL AUTO_INCREMENT,
        th_bw INT NOT NULL DEFAULT 0,
        th_sp INT NOT NULL DEFAULT 0,
        ac_bw INT NOT NULL DEFAULT 0,
        ac_sp INT NOT NULL DEFAULT 0,
        num_sv_total INT NOT NULL DEFAULT 0,
        num_sv_b INT NOT NULL DEFAULT 0,
        num_sv_s INT NOT NULL DEFAULT 0,
        num_sv_f INT NOT NULL DEFAULT 0,
        num_conn_total INT NOT NULL DEFAULT 0,
        file_add INT NOT NULL DEFAULT 0,
        PRIMARY KEY ( sys_time )
        )" ) ){
    echo "Table log_table creation failed: ( " . $db->errno . " ) " . $db->error . "\n";
}
else{
    echo "Table log_table created successful.\n";
}

if ( !$db->query ( " INSERT INTO $database.server_table ( bandwidth,space ) values ( '".BW_PER_SERVER."','".SPACE_PER_SERVER."' ) " )){
    echo "server_table initial data insert failed: ( " . $db->errno . " ) " . $db->error . "\n";
}
else{
    echo "Insert data to server_table successful.\n";
}
