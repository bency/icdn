<?php
// Last Update:2013/09/11 22:52:37 
class Sys_log{

    public static $systime = 0;
    public static $file_add = 0;

    public function __construct(){
        
        $stmt = Mysql::get_db()->prepare("select max(sys_time) from log_table");
        $stmt->execute();
        $stmt->bindColumn( 1, self::$systime );
        $stmt->fetch(PDO::FETCH_BOUND);
        $stmt->closeCursor();
        $db = null;
    }

    public function new_record (){

        $db = Mysql::get_db();
        $db->query("insert into log_table() values()");
        self::$systime = $db->lastInsertId();
        $db = null;
    }

    public function server_state_record($sys_time){

        $bw_sv = 0;
        $bw_conn = 0;
        foreach(Proc::$servers as $sv){
            $bw_sv += Env::$BW_PER_SERVER - $sv->bandwidth;
            $rec = "";
            foreach($sv->plist as $pid => $conn){
                $rec .= $pid . "," . count($conn) . ";";
            }
            Mysql::get_db()->query("insert into server_table (sys_time,sid,state,content) values('$sys_time','$sv->sid','$sv->state','$rec')");
        }
        foreach(Proc::$conn as $cn){
            $bw_conn += $cn->bandwidth;
        }
        unset($pid);
        /*
        echo "full:" . count(Proc::$full) . "\tspf:" . count(Proc::$spf) . "\tbwf:" . count(Proc::$bwf) . "\tops:" . count(Proc::$ops) . "\n";
        echo "total:" . count(Proc::$servers) . "\ttotal server bandwidth:$sum_sv_bandwidth\ttotal connection bandwidth:$sum_conn_bandwidth\n";
        */
        foreach(Proc::$conn as $cn){
            $pid[$cn->pid] = 1;
        }
        Mysql::get_db()->query( "
            update log_table set 
                bw_conn = '$bw_conn',
                bw_sv = '$bw_sv',
                num_sv_total = '".count(Proc::$servers)."',
                num_sv_s = '".count(Proc::$spf)."',
                num_sv_b = '".count(Proc::$bwf)."',
                num_sv_f = '".count(Proc::$full)."',
                num_conn_total = '".count(Proc::$conn)."',
                num_pid_total = '".count($pid)."',
                file_add = " . Proc::$file_add . "
            where sys_time=" . self::$systime );
        Proc::$file_add = 0;
    }
}
