<?php
// Last Update:2013/09/13 17:17:10 
// Bandwidth per server
class Env{
    
    public static $BW_PER_SERVER = 1000000;
    public static $BW_PER_PROGRAM = 200;
    public static $SPACE_PER_SERVER = 1000000;
    public static $SPACE_PER_PROGRAM = 800;
    public static $ARRIVAL_MUL = 1;
    public static $NUM_OF_PROGRAM;
    public static $ZIPF;
    public static $CONNECT_LIFE;
    public static $REFRESH_PERIOD;
    public static $DATABASE;
    public function __construct($filename){
        
        if(!file_exists($filename)){
            echo "ini file $filename is not exist\n";
            exit;
        }
        $setting = parse_ini_file($filename);
        self::$DATABASE = 'CDN_' . current(explode('.',$filename));
        self::$NUM_OF_PROGRAM = $setting['num_of_program'];
        self::$ZIPF = $setting['zipf'];
        self::$CONNECT_LIFE = $setting['connect_life'];
        self::$REFRESH_PERIOD = $setting['refresh_period'];
        if(isset($setting['bw_per_server'])){
            self::$BW_PER_SERVER = $setting['bw_per_server'];
        }
        if(isset($setting['bw_per_program'])){
            self::$BW_PER_PROGRAM = $setting['bw_per_program'];
        }
        if(isset($setting['space_per_server'])){
            self::$SPACE_PER_SERVER = $setting['space_per_server'];
        }
        if(isset($setting['space_per_program'])){
            self::$SPACE_PER_PROGRAM = $setting['space_per_program'];
        }
        if(isset($setting['arrival_mul'])){
            self::$ARRIVAL_MUL = $setting['arrival_mul'];
        }
    }
}
