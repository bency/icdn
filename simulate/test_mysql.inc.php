<?php
// Last Update:2013/08/13 15:48:00 
require_once 'debug_info.inc.php';
class Mysql {
    
    private $db = null;
    private $func_name = null;

    public function __construct( $func_name = null){
        
        $user = 'root';
        $passwd = 'ddyoinejjmj';
        $database = 'CDN';
        try{
            $this->db = new PDO( "mysql:host=localhost;dbname=" . $database, $user, $passwd );
        }
        catch ( PDOException $e ){
              print "Error!: " . $e->getMessage() . "<br/>";
              die();
        }
        $this->func_name = isset($func_name) ? $func_name : null;
        //Debug::func_start("link for $this->func_name");
    }

    public function get_db(){
        return $this->db;
    }

    public function __destruct(){
    
        $this->db = null;
        //Debug::func_end("link for $this->func_name");
    }
}
