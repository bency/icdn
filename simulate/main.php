<?php 
// Last Update:2013/09/13 17:16:23 
error_reporting(-1);
if($argc < 2){
    echo "Usage: php $argv[0] xxx.ini";
    exit;
}
require_once 'install.php';
require_once 'log.inc.php';
require_once 'constant.inc.php';
require_once 'function.inc.php';
require_once 'mysql.inc.php';
require_once 'connect.inc.php';
require_once 'server.inc.php';
require_once 'proc.inc.php';
$db = Mysql::get_db();
$start = time();
echo "================================================\n";
echo "資料庫名稱       \t" . Env::$DATABASE."\n";
echo "zipf             \t" . Env::$ZIPF."\n";
echo "流量基底         \t" . Env::$ARRIVAL_MUL * 8000 ."\n";
echo "預設每個檔案大小 \t" . Env::$SPACE_PER_PROGRAM . "\tMB\n";
echo "每條連線頻寬大小 \t" . Env::$BW_PER_PROGRAM . "\tKB/s\n";
echo "每台伺服器總容量 \t" . Env::$SPACE_PER_SERVER / 1000 . "\tGB\n";;
echo "每台伺服器總頻寬 \t" . Env::$BW_PER_SERVER / 1000 . "\tMB/s\n";;
echo "每條連線生存時間 \t" . Env::$CONNECT_LIFE . "\t秒\n";
echo "系統內檔案母體   \t" . Env::$NUM_OF_PROGRAM . "\t個檔案\n";
echo "可容納連線總數   \t" . Env::$BW_PER_SERVER / Env::$BW_PER_PROGRAM . "\t條連線\n";
echo "可容納檔案總數   \t" . Env::$SPACE_PER_SERVER / Env::$SPACE_PER_PROGRAM . "\t個檔案\n";
echo "================================================\n";
$db = null;

$proc = new Proc();
$log = new Sys_log();
$i = 0;
$arrival = 0;
for ($j = 0; $j < 1440; $j++ ){
    
    $log->new_record();
    $qt = arrival_in_minute( $j ) * Env::$ARRIVAL_MUL;
    printf("\n第%5d分鐘模擬開始\n產生%6d條連線\n",$j,$qt);
    
    $min_start = time();
    for( ; $i < $qt; $i++ ){
        
        $conn = new Connect();
        $arrival = $conn->new_conn($qt,$arrival);


        while( Proc::$cH->valid() and Proc::$cH->top()->depart < $arrival ){
        
            Debug::cur_state("d", Proc::$cH->top()->depart, getMicrotime() - $start);
            $proc->depart();
        }

        $proc->add_conn($conn);
        $conn = null;
        Debug::cur_state("a", $arrival, getMicrotime() - $start);
    }
    $i = 0;
    
    /*
    if( ($j + 1) % Env::$REFRESH_PERIOD == 0){
        $proc->stage_1();
        Debug::output("========");
        $proc->stage_3();
    }
    */
    $log->server_state_record($j);
    $time = getMicrotime() - $start;
    $min_time = time() - $min_start;
    printf("\n第%5d分鐘, 實際執行時間%5d秒, 時間比為:%2.2f\n",$j,$min_time,( $min_time / 60 ));
}

$time = time() - $start;
echo "Elapsed $time seconds\n";

$log = null;
