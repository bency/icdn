<?php 
// Last Update:2013/08/29 03:53:24 

class Connect {
    
    public $cnid = null;
    public $depart = null;
    public $arrival = null;
    public $space = null;
    public $bandwidth = null;
    public $pid = null;
    public $sid = null;

    public function __call( $name, $arguments){
    
        echo "注意! 注意! class connect 中 有個叫做'$name' 的方法還沒寫"
                     . implode(', ', $arguments). "\n";
    }

    public function __construct($id = null){
    
        if($id){
            
            $db = Mysql::get_db();
            $stmt = $db->prepare("select pid, sid, arrival, depart, bandwidth, space from connect_table where cnid=:cnid");
            $stmt->bindParam( ':cnid', $id );
            $stmt->execute();
            $this->cnid = $id;
            $stmt->bindColumn( "pid",$this->pid );
            $stmt->bindColumn( "sid",$this->sid );
            $stmt->bindColumn( "arrival",$this->arrival );
            $stmt->bindColumn( "depart",$this->depart );
            $stmt->bindColumn( "bandwidth",$this->bandwidth );
            $stmt->bindColumn( "space",$this->space );
            $stmt->fetch( PDO::FETCH_BOUND );
            $db = null;
        }else{
        
            $this->depart = Env::$CONNECT_LIFE;
            $this->space = Env::$SPACE_PER_PROGRAM;
            $this->bandwidth = Env::$BW_PER_PROGRAM;
        }
    }

    public function new_conn($qt,$sub){
        $this->pid = get_pid();
        $this->sid = 0;
        $this->arrival = exp_dist($qt/60) + $sub;
        $this->depart = $this->arrival + rand() % Env::$CONNECT_LIFE + 1;
        $this->space = Env::$SPACE_PER_PROGRAM;
        $this->bandwidth = Env::$BW_PER_PROGRAM;
        $this->cnid = 0;
        return $this->arrival;
    }
}
