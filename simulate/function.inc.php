<?php
// Last Update:2013/08/29 03:54:12 
function exp_dist($l){

    //return $l*exp(-$l*rand()/getrandmax());
    //return log ( 1 - rand() / ( getrandmax() + 1 ) ) / $l;
    return - ( 1 / $l ) * log ( 1 - rand() / ( getrandmax() + 1) );
}

function arrival_in_minute($m){
    $m = $m % 1440 + 1;
    return 8000 * ( 2 + cos ( 2 * pi() * $m / 1440 ) );
}

function get_pid(){

// Zipf's law preparation
// In order to decide which program to be requested
    
    static $zipf = array();

    static $Z_SIGMA = 0;

    // 倒數的ZIPF次方的總和
    if($Z_SIGMA == 0){

        echo "zipf initializing\n";
        for ( $i = 1; $i <= Env::$NUM_OF_PROGRAM; $i ++ ){
            $Z_SIGMA += pow( $i,-Env::$ZIPF/10 );
            $zipf[$i] = 0;
        }
        $zipf[0] = 0;
        $zipf[Env::$NUM_OF_PROGRAM] = 0;
        $sum = 0;

        for ($i = 1; $i <= Env::$NUM_OF_PROGRAM; $i ++){
            $sum += pow( $i, -Env::$ZIPF/10 );
            $zipf[$i] = $sum / $Z_SIGMA;
        }
    }

    // 取得亂數
    $p = rand() / ( getrandmax() + 1);

    // 計算屬於哪個pid的區間
    for($i = 0; $i < Env::$NUM_OF_PROGRAM; $i ++){
        if( $p > $zipf[$i] and $p <= $zipf[$i+1])
            return $i;
    }

}

function get_pid2($z){

// Zipf's law preparation
// In order to decide which program to be requested
    
    static $zs = 0;

    static $zipf = array();

    static $Z_SIGMA = 0;

    // 倒數的ZIPF次方的總和
    if($zs != $z){
        
        $zs = $z;
        $Z_SIGMA = 0;
    }
    if($Z_SIGMA == 0){

        echo "zipf initializing\n";
        for ( $i = 1; $i <= Env::$NUM_OF_PROGRAM; $i ++ ){
            $Z_SIGMA += pow( $i,-$zs);
            $zipf[$i] = 0;
        }
        $zipf[0] = 0;
        $zipf[Env::$NUM_OF_PROGRAM] = 0;
        $sum = 0;

        for ($i = 1; $i <= Env::$NUM_OF_PROGRAM; $i ++){
            $sum += pow( $i, -$zs);
            $zipf[$i] = $sum / $Z_SIGMA;
        }
    }

    // 取得亂數
    $p = rand() / ( getrandmax() + 1);

    // 計算屬於哪個pid的區間
    for($i = 0; $i < Env::$NUM_OF_PROGRAM; $i ++){
        if( $p > $zipf[$i] and $p <= $zipf[$i+1])
            return $i;
    }

}

function getMicrotime()
{
    list($usec, $sec) = explode(' ', microtime());
    return ((double)$usec + (double)$sec);
}
